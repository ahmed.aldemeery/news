<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\StoryController;
use App\Http\Controllers\CategoryController;

Route::get('/stories/all', [StoryController::class, 'all']);
Route::get('/stories/{story}/show', [StoryController::class, 'show']);

Route::middleware('guest:sanctum')->group(function () {
    // Auth
    Route::post('/auth/register', [AuthController::class, 'register']);
    Route::post('/auth/login', [AuthController::class, 'login']);
});

Route::middleware('auth:sanctum')->group(function () {
    // Auth
    Route::post('/auth/logout', [AuthController::class, 'logout']);

    // Users
    Route::get('/users', [UserController::class, 'index']);
    Route::put('/users/{user}', [UserController::class, 'update']);
    Route::patch('/users/{user}/password', [UserController::class, 'changePassword']);
    Route::patch('/users/{user}/entrust', [UserController::class, 'entrust']);
    Route::patch('/users/{user}/distrust', [UserController::class, 'distrust']);

    // Categories
    Route::apiResource('/categories', CategoryController::class)->except('show');

    // Stories
    Route::apiResource('/stories', StoryController::class);
    Route::patch('/stories/{story}/publish', [StoryController::class, 'publish']);
    Route::patch('/stories/{story}/unpublish', [StoryController::class, 'unpublish']);
});
