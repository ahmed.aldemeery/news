<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\User;
use App\Models\Story;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class StorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        foreach (User::cursor() as $user) {
            $stories = Story::factory(5)->make();
            $user->stories()->saveMany($stories);

            foreach ($stories as $story) {
                $story->categories()->attach(Category::inRandomOrder()->take(3)->get());
            }
        }
    }
}
