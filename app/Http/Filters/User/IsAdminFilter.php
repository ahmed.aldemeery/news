<?php

namespace App\Http\Filters\User;

use Aldemeery\Sieve\Filter;
use Illuminate\Database\Eloquent\Builder;

class IsAdminFilter extends Filter
{
    /**
     * Values mappings.
     *
     * @var array
     */
    protected $mappings = [
        "1" => true,
        "0" => false,
        "true" => true,
        "false" => false,
        "yes" => true,
        "no" => false,
        "y" => true,
        "n" => false,
        "t" => true,
        "f" => false,
    ];

    /**
     * Filter records based on a given value.
     *
     * @param \Illuminate\Database\Eloquent\Builder $builder Eloquent builder instance.
     * @param string $value The resolved value of the filtration key sent in the query string.
     *
     * @return void
     */
    public function filter(Builder $builder, $value)
    {
        if ($value) {
            $builder->where('is_admin', $value);
        }
    }
}
