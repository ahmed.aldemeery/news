<?php

namespace App\Http\Controllers;

use App\Http\Requests\ChangePasswordRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\UpdateUserRequest;
use App\Http\Resources\User as UserResource;
use Illuminate\Auth\Middleware\Authorize;

class UserController extends Controller
{
    public function index(Request $request)
    {
        $this->authorize('viewAny', User::class);

        $users = User::filter($request)->paginate();

        return UserResource::collection($users);
    }

    public function update(UpdateUserRequest $request, User $user)
    {
        $this->authorize('update', $user);

        $data = $request->validated();
        $user->update($data);

        return new UserResource($user);
    }

    public function changePassword(ChangePasswordRequest $request, User $user)
    {
        $this->authorize('update', $user);

        $user->password = $request->password;
        $user->save();

        return new UserResource($user);
    }

    /**
     * Convert a given user into an admin.
     */
    public function entrust(User $user)
    {
        $this->authorize('entrust', User::class);

        $user->is_admin = true;

        $user->save();

        return new UserResource($user);
    }

    /**
     * Convert a given user into a regular user.
     */
    public function distrust(User $user)
    {
        $this->authorize('distrust', User::class);

        $user->is_admin = false;

        $user->save();

        return new UserResource($user);
    }
}
