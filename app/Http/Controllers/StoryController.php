<?php

namespace App\Http\Controllers;

use App\Models\Story;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Requests\CreateStoryRequest;
use App\Http\Requests\UpdateStoryRequest;
use App\Http\Resources\Story as StoryResource;
use Illuminate\Auth\Middleware\Authorize;

class StoryController extends Controller
{
    public function index(Request $request)
    {
        $this->authorize('viewAny', Story::class);

        $stories = Story::filter($request)->with('user', 'categories')->paginate();

        return StoryResource::collection($stories);
    }

    public function all(Request $request)
    {
        $stories = Story::filter($request)
            ->published()
            ->with('user', 'categories')
            ->paginate();

        return StoryResource::collection($stories);
    }

    public function store(CreateStoryRequest $request)
    {
        $this->authorize('create', Story::class);

        $story = $request->user()->stories()->create($request->validated());
        $story->categories()->attach($request->categories);

        $story->load('user', 'categories');

        return new StoryResource($story);
    }

    public function show(Story $story)
    {
        $this->authorize('view', $story);

        $story->load('user', 'categories');

        return new StoryResource($story);
    }

    public function update(UpdateStoryRequest $request, Story $story)
    {
        $this->authorize('update', $story);

        $story->update($request->validated());
        $story->categories()->sync($request->categories);

        $story->load('user', 'categories');

        return new StoryResource($story);
    }

    public function destroy(Story $story)
    {
        $this->authorize('delete', $story);

        $story->delete();

        return response()->json(null, 204);
    }

    public function publish(Story $story)
    {
        $this->authorize('publish', $story);

        $story->update(['is_published' => true]);

        return new StoryResource($story);
    }

    public function unpublish(Story $story)
    {
        $this->authorize('unpublish', $story);

        $story->update(['is_published' => false]);

        return new StoryResource($story);
    }
}
