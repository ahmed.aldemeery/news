<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class Story extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            "id" => $this->id,
            "title" => $this->title,
            "content" => $this->content,
            "is_published" => (bool) $this->is_published,
            "user" => new User($this->whenLoaded('user')),
            "categories" => Category::collection($this->whenLoaded('categories')),
        ];
    }
}
