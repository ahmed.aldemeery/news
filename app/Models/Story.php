<?php

namespace App\Models;

use App\Models\User;
use App\Models\Category;
use Illuminate\Database\Eloquent\Model;
use Aldemeery\Sieve\Concerns\Filterable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Story extends Model
{
    use HasFactory;
    use Filterable;

    protected $fillable = [
        'title',
        'content',
        'is_published',
    ];

    protected $casts = [
        'is_published' => 'bool',
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function categories(): BelongsToMany
    {
        return $this->belongsToMany(Category::class);
    }

    public function scopePublished(Builder $builder): void
    {
        $builder->where('is_published', true);
    }

    protected function filters()
    {
        return [
            'title' => \App\Http\Filters\Story\TitleFilter::class,
            'content' => \App\Http\Filters\Story\ContentFilter::class,
            'is_published' => \App\Http\Filters\Story\IsPublishedFilter::class,
            'category' => \App\Http\Filters\Story\CategoryFilter::class,
            'user' => \App\Http\Filters\Story\UserFilter::class,
        ];
    }
}
