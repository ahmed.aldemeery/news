<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use App\Models\Story;
use Laravel\Sanctum\HasApiTokens;
use Aldemeery\Sieve\Concerns\Filterable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens;
    use HasFactory;
    use Notifiable;
    use Filterable;

    protected $fillable = [
        'name',
        'email',
        'password',
        'is_admin',
    ];

    protected $hidden = [
        'password',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
        'password' => 'hashed',
        'is_admin' => 'bool',
    ];

    public function stories(): HasMany
    {
       return $this->hasMany(Story::class);
    }

    public function isAdmin(): bool
    {
        return $this->is_admin;
    }

    protected function filters()
    {
        return [
            'name' => \App\Http\Filters\User\NameFilter::class,
            'email' => \App\Http\Filters\User\EmailFilter::class,
            'is_admin' => \App\Http\Filters\User\IsAdminFilter::class,
        ];
    }
}
