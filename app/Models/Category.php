<?php

namespace App\Models;

use Aldemeery\Sieve\Concerns\Filterable;
use App\Models\Story;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Category extends Model
{
    use HasFactory;
    use Filterable;

    protected $fillable = [
        'name',
    ];

    public function stories(): BelongsToMany
    {
        return $this->belongsToMany(Story::class);
    }

    protected function filters()
    {
        return [
            'name' => \App\Http\Filters\Category\NameFilter::class,
        ];
    }
}
