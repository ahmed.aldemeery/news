<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    public function viewAny(User $user)
    {
        return $user->isAdmin();
    }

    public function update(User $authenticated, User $user)
    {
        return $authenticated->is($user);
    }

    public function entrust(User $user)
    {
        return $user->isAdmin();
    }

    public function distrust(User $user)
    {
        return $user->isAdmin();
    }
}
